package com.mastercard.mcbp.bhapi;

import com.mastercard.mobile_api.bytes.ByteArray;
import com.mastercard.mobile_api.utils.json.ByteArrayTransformer;

import flexjson.JSON;
import flexjson.JSONSerializer;

/**
 * Created by dima on 16.04.16.
 */
public abstract class BaseResponse {

    //todo add other common fields

    @JSON(name = "resultCode")
    protected int mResultCode;

    public int getResultCode() {
        return mResultCode;
    }

    public void setResultCode(int mResultCode) {
        this.mResultCode = mResultCode;
    }

    public String toJsonString() {
        JSONSerializer serializer = new JSONSerializer();
        serializer.exclude("*.class").transform(new ByteArrayTransformer(), ByteArray.class);
        return serializer.serialize(this);
    }
}
