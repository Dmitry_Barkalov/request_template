package com.mastercard.mcbp.bhapi;

import com.mastercard.mcbp.utils.http.AndroidHttpGetRequest;
import com.mastercard.mcbp.utils.http.HttpGetRequest;

import flexjson.JSON;
import flexjson.JSONDeserializer;

/**
 * Created by dima on 16.04.16.
 */
public class ExampleGetRequest extends BaseRequest<HttpGetRequest, ResponseHandler<ExampleGetRequest.Response>> {

    private final int mParam1;
    private final int mParam2;

    public ExampleGetRequest(String host, int param1, int param2) {
        super(host);
        mParam1 = param1;
        mParam2 = param2;
    }


    @Override
    public HttpGetRequest buildHttpRequest() {
        return new AndroidHttpGetRequest().withUrl(buildUrl());
    }

    @Override
    public ResponseHandler<Response> getResponseHandler() {
        return new Handler();
    }

    @Override
    protected String buildUrl() {
        StringBuilder sb = new StringBuilder();
        sb.append(getHost());
        sb.append("path/");
        sb.append("?param1=").append(String.valueOf(mParam1));
        sb.append("?param2=").append(String.valueOf(mParam2));
        return sb.toString();
    }

    /**
     * response data
     */
    public static class Response extends BaseResponse {

        private Response() {
        }

        @JSON(name = "outParam")
        private int outParam;

        public Response(int outParam) {
            this.outParam = outParam;
        }

        public int getOutParam() {
            return outParam;
        }

        public void setOutParam(int outParam) {
            this.outParam = outParam;
        }
    }

    /**
     * parse response string to Response
     */
    public static class Handler implements ResponseHandler<Response> {
        @Override
        public Response parseResponse(String string) throws RuntimeException {
            JSONDeserializer<Response> deserializer = new JSONDeserializer<>();
            return deserializer.deserialize(string, Response.class);
        }
    }

}
