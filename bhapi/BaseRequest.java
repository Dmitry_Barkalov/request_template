package com.mastercard.mcbp.bhapi;

/**
 * Created by dima on 16.04.16.
 */

import flexjson.JSON;
import flexjson.JSONSerializer;

/**
 *  R - http request - send by http factory
 *  H - response handler
 */
public abstract class BaseRequest<R, H> {

    //todo add common request fields

    @JSON(include = false)
    private String mHost;

    protected BaseRequest(String host) {
        this.mHost = host;
    }

    @JSON(include = false)
    protected  String getHost(){
        return mHost;
    };

    abstract protected  String buildUrl();

    public abstract R buildHttpRequest();

    public abstract H getResponseHandler();

    public String toJson(){
        JSONSerializer serializer = new JSONSerializer();
        serializer.exclude("*.class");
        //.transform(new ByteArrayTransformer(), ByteArray.class);
        return serializer.serialize(this);
    }
}
