package com.mastercard.mcbp.bhapi;

import com.mastercard.mobile_api.bytes.ByteArray;

/**
 * Created by dima on 16.04.16.
 */
public interface ResponseHandler<R extends BaseResponse> {
    R parseResponse(String  string) throws RuntimeException;
}
