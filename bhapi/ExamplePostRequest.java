package com.mastercard.mcbp.bhapi;

import com.mastercard.mcbp.utils.http.AndroidHttpPostRequest;
import com.mastercard.mcbp.utils.http.HttpPostRequest;

import flexjson.JSON;
import flexjson.JSONDeserializer;

/**
 * Created by dima on 16.04.16.
 */
public class ExamplePostRequest extends BaseRequest<HttpPostRequest, ResponseHandler<ExamplePostRequest.Response>> {

    @JSON(name = "name")
    private String name;

    @JSON(name = "id")
    private String id;

    private void ExamplePostRequest() {
    }

    public ExamplePostRequest(String host, String param1, String param2) {
        super(host);
        name = param1;
        id = param2;
    }


    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @JSON(include = false)
    @Override
    public HttpPostRequest buildHttpRequest() {
        String jsonStr = toJson();
        return new AndroidHttpPostRequest().withUrl(buildUrl()).withRequestData(jsonStr);
    }

    @Override
    protected String buildUrl() {
        StringBuilder sb = new StringBuilder(getHost());
        sb.append("path/path/");
        return sb.toString();
    }

    @JSON(include = false)
    @Override
    public ResponseHandler<Response> getResponseHandler() {
        return new Handler();
    }

    /**
     * response data
     */
    public static class Response extends BaseResponse {

        public Response() {

        }

        @JSON(name = "param")
        private String outParam1;

        public String getParam() {
            return outParam1;
        }

        public void setParam(String outParam) {
            this.outParam1 = outParam;
        }

        public Response(String outParam1) {
            this.outParam1 = outParam1;
        }
    }


    /**
     * parse response string to Response
     */
    public static class Handler implements ResponseHandler<Response> {
        @Override
        public Response parseResponse(String string) throws RuntimeException {
            JSONDeserializer<Response> deserializer = new JSONDeserializer<>();
            return deserializer.deserialize(string, Response.class);
        }
    }

}
