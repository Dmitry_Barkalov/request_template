package com.mastercard.mcbp.bhapi;

import android.test.AndroidTestCase;

import com.mastercard.mcbp.utils.http.HttpGetRequest;
import com.mastercard.mcbp.utils.http.HttpPostRequest;

/**
 * Created by dima on 16.04.16.
 */
public class RequestTest extends AndroidTestCase {


    public void testGetRequest() {

        ExampleGetRequest r = new ExampleGetRequest("host/", 11, 22);
        HttpGetRequest httpRequest = r.buildHttpRequest();

        assertEquals("host/path/?param1=11?param2=22", httpRequest.getUrl());

        ResponseHandler<ExampleGetRequest.Response> handler = r.getResponseHandler();


        ExampleGetRequest.Response response1 = new ExampleGetRequest.Response(55);
        assertEquals("{\"outParam\":55,\"resultCode\":0}", response1.toJsonString());

        ExampleGetRequest.Response response = handler.parseResponse("{\"outParam\":55,\"resultCode\":200}");
        assertEquals(55, response.getOutParam());
        assertEquals(200, response.getResultCode());

        // exception
        try {
            ExampleGetRequest.Response responseError = handler.parseResponse("dfgfdgdfg");
            fail("expect throw ClassCastException");
        } catch (Exception e) {
            assertEquals(ClassCastException.class, e.getClass());
        }

    }

    public void testPostRequest() {

        ExamplePostRequest r = new ExamplePostRequest("host22/", "str1111", "str2222");
        HttpPostRequest httpRequest = r.buildHttpRequest();

        assertEquals("host22/path/path/", httpRequest.getUrl());
        assertEquals("{\"id\":\"str2222\",\"name\":\"str1111\"}", httpRequest.getRequestData());

        ResponseHandler<ExamplePostRequest.Response> handler = r.getResponseHandler();
        ExamplePostRequest.Response response = handler.parseResponse("{\"param\":\"azz\"}");
        assertEquals("azz", response.getParam());

    }

}
